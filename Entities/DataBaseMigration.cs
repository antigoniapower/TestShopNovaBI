﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Entities.Books;
using Entities.Disks;

namespace Entities
{
    public class DataBaseMigration
    {
        public static void InsertNewBook(string BookName)
        {
            using (var db = new ShopContext())
            {
                var Book = new Book { Name = BookName };
                db.Books.Add(Book);
                db.SaveChanges();
            }
        }
    }

    public class ShopContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Disk> Disks { get; set; }
    }
}
