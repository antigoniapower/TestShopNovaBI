﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Books
{
    [Table("Book")]
    public class Book: AbstractCatalogItem
    {
        public int Pagecount { get; set; }
        public override string EntityNominative { get { return "Книга"; } }
        protected override string Describe()
        {
            return base.Describe() +
                "\nКоличество страниц: " + Pagecount;
        }
    }
}
