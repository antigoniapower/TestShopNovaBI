﻿using System.ComponentModel.DataAnnotations.Schema;


namespace Entities.Books
{
    [Table("SoftwareBook")]
    public class SoftwareBook:Book
    {
        public string ProgrammingLanguage { get; set; }
        public override string EntityNominative { get { return base.EntityNominative + " по программированию"; } }
        protected override string Describe()
        {
            return base.Describe() +
                "\nЯзык программирования: " + ProgrammingLanguage;
        }
    }
}
