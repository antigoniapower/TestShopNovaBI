﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Books
{
    [Table("EsotericBook")]
    public class EsotericBook:Book
    {
        public int MinReaderAge { get; set; }
        public override string EntityNominative { get { return base.EntityNominative + " по эзотерике"; } }
        protected override string Describe()
        {
            return base.Describe() +
                "\nМинимальный возраст читателя: " + MinReaderAge;
        }
    }
}
