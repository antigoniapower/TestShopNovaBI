﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Books
{
    [Table("CookBook")]
    public class CookBook:Book
    {
        public string MainIngridient { get; set; }
        public override string EntityNominative { get { return base.EntityNominative + " по кулинарии"; } }
        protected override string Describe()
        {
            return base.Describe() +
                "\nОсновной ингредиент: " + MainIngridient;
        }
    }
}
