﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class AbstractCatalogItem
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public string Barcode { get; set; }
        public string Name { get; set; }
        public virtual string EntityNominative { get { return "Все"; } }
        public string Description { get { return Describe(); } }
        protected virtual string Describe ()
        {
            return
                EntityNominative + 
                "\nId: " + Id +
                "\nНазвание: " + Name +
                "\nЦена: " + Price +
                "\nШтрих-код: " + Barcode;
        }
    }
}
