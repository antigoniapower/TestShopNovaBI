﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Disks
{
    [Table("CD")]
    public class CD: Disk
    {
        public override string EntityNominative { get { return base.EntityNominative +" CD"; } }
    }
}
