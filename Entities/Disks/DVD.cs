﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Disks
{
    [Table("DVD")]
    public class DVD: Disk
    {
        public override string EntityNominative { get { return base.EntityNominative + " DVD"; } }
    }
}
