﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Disks
{
    public enum DiskContent
    {
        Music,
        Video, 
        Software
    }
    [Table("Disk")]
    public class Disk: AbstractCatalogItem
    {
        public DiskContent Content { get; set; }
        public override string EntityNominative { get { return "Диск "; } }
        protected override string Describe()
        {
            return base.Describe() +
                "\nСодержимое: " + Content;
        }
    }
}
