﻿using Entities.Books;
using Entities.Disks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{

    public static class ClassesProperties
    {
        public static List<Type> allTypes = new List<Type>()
        {
            { typeof(AbstractCatalogItem)},
            { typeof(Book)},
            { typeof(Disk)},
            { typeof(CookBook)},
            { typeof(EsotericBook)},
            { typeof(SoftwareBook)},
            { typeof(CD)},
            { typeof(DVD)}
        };

        public static Dictionary<Type, string> Normatives { get { return getNormatives(); } }

        static Dictionary<Type, string> getNormatives()
        {
            Dictionary<Type, string> categories = new Dictionary<Type, string>();
            foreach (Type type in ClassesProperties.allTypes)
            {
                AbstractCatalogItem instance = (AbstractCatalogItem)Activator.CreateInstance(type);
                categories.Add(type, instance.EntityNominative);
            }

            return categories;
        }
    }
}
