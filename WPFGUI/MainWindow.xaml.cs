﻿using DBMigration.Data;
using System.Windows;
using System.Data.Entity;
using System;
using Entities;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using Entities.Books;
using Entities.Disks;

namespace WPFGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ShopContext db;
        public MainWindow()
        {
            InitializeComponent();

            db = new ShopContext();

            InitGrShopItems();
            InitCbCategory();

            this.Closing += MainWindow_Closing;
        }

        private void InitCbCategory()
        {
            cbCategory.ItemsSource = ClassesProperties.Normatives.Values; //перечисление нормативных форм классов
        }

        private void InitGrShopItems()
        {
            db.CatalogItems.Load(); //загружаем данные
            grdShopItems.ItemsSource = db.CatalogItems.Local.ToBindingList(); //устанавливаем привязку к кэшу
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            db.Dispose();
        }

        private void cbCategory_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            string Value = (string)(sender as ComboBox).SelectedValue;
            Type type = ClassesProperties.Normatives.FirstOrDefault().Key;//определяем выбранный тип категории
            if (ClassesProperties.Normatives.ContainsValue(Value))
                type = ClassesProperties.Normatives.FirstOrDefault(p => p.Value == Value).Key;

            //присоединяем нужный источник данных
            BindDbSetByType(type);
            //меняем колонки в таблице местами
            ChangeColumnOrder(grdShopItems);
        }

        private void BindDbSetByType(Type type)
        {
            if (type == typeof(AbstractCatalogItem))
            {
                db.CatalogItems.Load();
                grdShopItems.ItemsSource = db.CatalogItems.Local.ToBindingList();
                grdShopItems.CanUserAddRows = false;
            }
            else if (type == typeof(Book))
            {
                db.Books.Load();
                grdShopItems.ItemsSource = db.Books.Local.ToBindingList();
                grdShopItems.CanUserAddRows = false;
            }
            else if (type == typeof(Disk))
            {
                db.Disks.Load();
                grdShopItems.ItemsSource = db.Disks.Local.ToBindingList();
                grdShopItems.CanUserAddRows = false;
            }
            else if (type == typeof(CookBook))
            {
                db.CookBooks.Load();
                grdShopItems.ItemsSource = db.CookBooks.Local.ToBindingList();
                grdShopItems.CanUserAddRows = true;
            }
            else if (type == typeof(EsotericBook))
            {
                db.EsotericBooks.Load();
                grdShopItems.ItemsSource = db.EsotericBooks.Local.ToBindingList();
                grdShopItems.CanUserAddRows = true;
            }
            else if (type == typeof(SoftwareBook))
            {
                db.SoftwareBooks.Load();
                grdShopItems.ItemsSource = db.SoftwareBooks.Local.ToBindingList();
                grdShopItems.CanUserAddRows = true;
            }
            else if (type == typeof(CD))
            {
                db.CDs.Load();
                grdShopItems.ItemsSource = db.CDs.Local.ToBindingList();
                grdShopItems.CanUserAddRows = true;
            }
            else if (type == typeof(DVD))
            {
                db.DVDs.Load();
                grdShopItems.ItemsSource = db.DVDs.Local.ToBindingList();
                grdShopItems.CanUserAddRows = true;
            }
            else
            {
                db.CatalogItems.Load();
                grdShopItems.ItemsSource = db.CatalogItems.Local.ToBindingList();
                grdShopItems.CanUserAddRows = false;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            db.SaveChanges();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (grdShopItems.SelectedItems.Count > 0)
            {
                AbstractCatalogItem item = grdShopItems.SelectedItem as AbstractCatalogItem;
                if (item != null)
                {
                    db.CatalogItems.Remove(item);
                }
            }
        }

        private void grdShopItems_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string headername = e.Column.Header.ToString();
            if (headername.Equals("Id", StringComparison.OrdinalIgnoreCase) ||
                headername.Equals("Description", StringComparison.OrdinalIgnoreCase) &&
                grdShopItems.ItemsSource != db.CatalogItems.Local.ToBindingList())
                e.Cancel = true;

            ChangeColumnHeaders(e, headername);

        }

        private static void ChangeColumnHeaders(DataGridAutoGeneratingColumnEventArgs e, string headername)
        {
            switch (headername)
            {
                case "Name":
                    e.Column.Header = "Наименование";
                    e.Column.DisplayIndex = 0;
                    break;

                case "Price":
                    e.Column.Header = "Цена";
                    e.Column.DisplayIndex = 1;
                    break;

                case "Barcode":
                    e.Column.Header = "Штрих код";
                    e.Column.DisplayIndex = 2;
                    break;

                case "EntityNominative":
                    e.Column.Header = "Категория";
                    break;

                case "Description":
                    e.Column.Header = "Описание";
                    break;

                case "Pagecount":
                    e.Column.Header = "Число страниц";
                    break;

                case "MainIngridient":
                    e.Column.Header = "Основной ингридиент";
                    break;

                case "ProgrammingLanguage":
                    e.Column.Header = "Язык программирования";
                    break;

                case "Content":
                    e.Column.Header = "Содежимое";
                    break;

                case "MinReaderAge":
                    e.Column.Header = "Минимальный возраст читателя";
                    break;
            }
        }

        private void ChangeColumnOrder(DataGrid grd)
        {
            foreach (var item in grd.Columns)
            {
                string headername = item.Header.ToString();
                switch (headername)
                {
                    case "Наименование":
                        item.DisplayIndex = 0;
                        break;

                    case "Цена":
                        item.DisplayIndex = 1;
                        break;

                    case "Штрих код":
                        item.DisplayIndex = 2;
                        break;

                    case "Категория":
                        item.DisplayIndex = 3;
                        break;
                }
                
            }
        }
    }
}
