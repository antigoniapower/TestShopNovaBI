﻿using DBMigration.Data;
using DBMigration.TestData;
using System;


namespace ConsoleGUI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Каталог магазина Nova BI" + Environment.NewLine);

            using (var context = new ShopContext())
            {
                DataExchanger exch = new DataExchanger(context);
                var items = exch.ListAllCatalog();
                foreach (var item in items)
                    Console.WriteLine(item.Description + Environment.NewLine);
            }

            Console.WriteLine("Нажмите любую клавишу");
            Console.ReadKey();
        }
    }
}
