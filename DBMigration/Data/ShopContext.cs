﻿using System.Data.Entity;
using Entities.Books;
using Entities.Disks;
using Entities;
using System;
using System.Collections.Generic;

namespace DBMigration.Data
{
    public class ShopContext : DbContext
    {
        public DbSet<AbstractCatalogItem> CatalogItems { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Disk> Disks { get; set; }
        public DbSet<CookBook> CookBooks { get; set; }
        public DbSet<EsotericBook> EsotericBooks { get; set; }
        public DbSet<SoftwareBook> SoftwareBooks { get; set; }
        public DbSet<CD> CDs { get; set; }
        public DbSet<DVD> DVDs { get; set; }
    }

    public static class ShopContextExtensions
    {
        public static dynamic GetDbSetByType(this ShopContext db, Type type)
            {
            if (type == typeof(AbstractCatalogItem))
                return db.CatalogItems;
            else if(type == typeof(Book))
                return db.Books;
            else if (type == typeof(Disk))
                return db.Disks;
            else if (type == typeof(CookBook))
                return db.CookBooks;
            else if (type == typeof(EsotericBook))
                return db.EsotericBooks;
            else if (type == typeof(SoftwareBook))
                return db.SoftwareBooks;
            else if (type == typeof(CD))
                return db.CDs;
            else if (type == typeof(DVD))
                return db.DVDs;
            else
                return db.CatalogItems;
            }
    }

}
