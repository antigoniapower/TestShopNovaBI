﻿using System;
using System.Collections.Generic;
using Entities.Books;
using Entities.Disks;
using Entities;
using System.Linq;

namespace DBMigration.Data
{   
    public class DataExchanger
    {
        ShopContext db;
        public DataExchanger(ShopContext db)
        {
            this.db = db;

            delegates = new Dictionary<Type, InsertDelegate>()
            {{ typeof(CookBook), (item) => db.CookBooks.Add((CookBook)item)},
            { typeof(EsotericBook), (item) => db.EsotericBooks.Add((EsotericBook)item)},
            { typeof(SoftwareBook), (item) => db.SoftwareBooks.Add((SoftwareBook)item)},
            { typeof(CD), (item) => db.CDs.Add((CD)item)},
            { typeof(DVD), (item) => db.DVDs.Add((DVD)item)} };
        }
        delegate void InsertDelegate(AbstractCatalogItem item);

        Dictionary<Type, InsertDelegate> delegates = new Dictionary<Type, InsertDelegate>();

        public void InsertCatalogItem(AbstractCatalogItem item)
        {
            Type type = item.GetType();
            if (delegates.ContainsKey(type))
            {
                InsertDelegate ins = delegates[type];
                ins(item);
            }
        }

        public void InsertCatalogItemList(IEnumerable<AbstractCatalogItem> items)
        {
            foreach (AbstractCatalogItem item in items)
              InsertCatalogItem(item);
        }

        public IEnumerable<AbstractCatalogItem> ListAllCatalog()
        {
            var query = db.CatalogItems;
            var result = query.ToList().OrderBy(p => p.EntityNominative);

            return result;           
        }

    }

}
