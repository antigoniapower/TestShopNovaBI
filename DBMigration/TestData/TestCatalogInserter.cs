﻿using DBMigration.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBMigration.TestData
{
    public class TestCatalogInserter
    {
        public static void Go()
        {
            using (var context = new ShopContext())
            {
                DataExchanger exch = new DataExchanger(context);
                exch.InsertCatalogItemList(TestCatalog.CatalogItems);
                context.SaveChanges();
            }
        }
    }
}
