﻿using Entities;
using Entities.Books;
using Entities.Disks;
using System.Collections.Generic;

namespace DBMigration
{
    public class TestCatalog
    {
        public static IEnumerable<AbstractCatalogItem> CatalogItems = new List<AbstractCatalogItem>()
        {
        new CookBook() { Name = "Поваренная книга нищеброда (проект 1500)", Barcode = "1111", Pagecount = 45, Price = 50M, MainIngridient = "картошка" },
        new CookBook() { Name = "Книга о вкусной и здоровой пище", Barcode = "1112", Pagecount = 300, Price = 400M, MainIngridient = "макароны" },
        new EsotericBook() { Name = "Как заработать много денег и не болеть", Barcode = "1113", Pagecount = 300, Price = 400M, MinReaderAge = 12},
        new EsotericBook() { Name = "ОШО Горчичное зерно, книга 128", Barcode = "1114", Pagecount = 100, Price = 500M, MinReaderAge = 100},
        new EsotericBook() { Name = "Вестник школы дальнейшего энергоинформационного развития, выпуск 15", Barcode = "1115", Pagecount = 800, Price = 500M, MinReaderAge = 100},
        new SoftwareBook() { Name = "Чистый код. Создание, анализ и рефакторинг", Barcode = "1116", Pagecount = 464, Price = 700M, ProgrammingLanguage = "Java"},
        new SoftwareBook() { Name = "Паттерны проектирования", Barcode = "1117", Pagecount = 656, Price = 977M, ProgrammingLanguage = "Java"},
        new CD () {Name = "Guano Apes. Walking On A Thin Line", Barcode = "1118", Price = 100M, Content = DiskContent.Music},
        new CD () {Name = "SOAD. Stole This Album!", Barcode = "1119", Price = 120M, Content = DiskContent.Music},
        new DVD () {Name = "Windows XP + Сборник программ", Barcode = "1120", Price = 30M, Content = DiskContent.Software},
        new DVD () {Name = "Отряд самоубийц", Barcode = "1121", Price = 300M, Content = DiskContent.Video},
        new DVD () {Name = "Невероятная истоиря Бенджамина Баттона", Barcode = "1122", Price = 320M, Content = DiskContent.Video},
        };   
    }
}
