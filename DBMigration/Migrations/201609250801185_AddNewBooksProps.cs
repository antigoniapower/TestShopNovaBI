namespace DBMigration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewBooksProps : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CookBook", "MainIngridient", c => c.String());
            AddColumn("dbo.EsotericBook", "MinReaderAge", c => c.Int(nullable: false));
            AddColumn("dbo.SoftwareBook", "ProgrammingLanguage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SoftwareBook", "ProgrammingLanguage");
            DropColumn("dbo.EsotericBook", "MinReaderAge");
            DropColumn("dbo.CookBook", "MainIngridient");
        }
    }
}
