namespace DBMigration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AbstractCatalogItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Barcode = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Book",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Pagecount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbstractCatalogItems", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Disk",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Content = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbstractCatalogItems", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.CD",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Disk", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.CookBook",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Book", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.DVD",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Disk", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.EsotericBook",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Book", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.SoftwareBook",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Book", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SoftwareBook", "Id", "dbo.Book");
            DropForeignKey("dbo.EsotericBook", "Id", "dbo.Book");
            DropForeignKey("dbo.DVD", "Id", "dbo.Disk");
            DropForeignKey("dbo.CookBook", "Id", "dbo.Book");
            DropForeignKey("dbo.CD", "Id", "dbo.Disk");
            DropForeignKey("dbo.Disk", "Id", "dbo.AbstractCatalogItems");
            DropForeignKey("dbo.Book", "Id", "dbo.AbstractCatalogItems");
            DropIndex("dbo.SoftwareBook", new[] { "Id" });
            DropIndex("dbo.EsotericBook", new[] { "Id" });
            DropIndex("dbo.DVD", new[] { "Id" });
            DropIndex("dbo.CookBook", new[] { "Id" });
            DropIndex("dbo.CD", new[] { "Id" });
            DropIndex("dbo.Disk", new[] { "Id" });
            DropIndex("dbo.Book", new[] { "Id" });
            DropTable("dbo.SoftwareBook");
            DropTable("dbo.EsotericBook");
            DropTable("dbo.DVD");
            DropTable("dbo.CookBook");
            DropTable("dbo.CD");
            DropTable("dbo.Disk");
            DropTable("dbo.Book");
            DropTable("dbo.AbstractCatalogItems");
        }
    }
}
